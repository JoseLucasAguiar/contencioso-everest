<?php


	session_start();
	if((!isset ($_SESSION['login-user']) == true) and (!isset ($_SESSION['senha-user']) == true))
	{
	unset($_SESSION['login-user']);
	unset($_SESSION['senha-user']);
	header('location:Login.php');
	}
	
	$logado = $_SESSION['login-user'];
	
	include_once("Administracao\ProcessoController.php");
	include_once("Administracao\Mensagem.php");
	include_once("Administracao\UsuarioController.php");
	include_once("Administracao\Usuario.php");

	$processoController = new ProcessoController();
	$mensagem = new Mensagem();
	$usuarioController = new UsuarioController();
	$usuario = new Usuario();


	$id = $_POST["ID_PROC"];
	$msg = $_POST["MENSAGEM_PROCESSO"];
	$titulo = $_POST["MENS_TITULO"];
	$usuario = $usuarioController->selecionaIdPeloLogin($logado);
	$mensagem->setIdProcFK($id);
	$mensagem->setMensagem($msg);
	$mensagem->setTitulo($titulo);
	$mensagem->setIdUsuarioFK($usuario[0]);
	$processoController->insereMensagemDoProcesso($mensagem);
	//header("location: DetalhesProcesso.php?id=".$id);
	header("location: EditarProcesso.php?id=".$id);
?>