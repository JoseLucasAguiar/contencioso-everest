-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 04-Out-2018 às 22:18
-- Versão do servidor: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistema`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_advogado`
--

CREATE TABLE `tbl_advogado` (
  `ID_ADV` int(11) NOT NULL,
  `OAB_REG_ADV` varchar(50) NOT NULL,
  `ID_USER_FK` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tbl_advogado`
--

INSERT INTO `tbl_advogado` (`ID_ADV`, `OAB_REG_ADV`, `ID_USER_FK`) VALUES
(10, '1234', 26),
(11, 'a', 25);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_advogado_processo`
--

CREATE TABLE `tbl_advogado_processo` (
  `ID_ADV` int(11) NOT NULL,
  `ID_PROC` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tbl_advogado_processo`
--

INSERT INTO `tbl_advogado_processo` (`ID_ADV`, `ID_PROC`) VALUES
(10, 13);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_detalhe_processo`
--

CREATE TABLE `tbl_detalhe_processo` (
  `ID_DET_PROC` int(11) NOT NULL,
  `RECLAMANTE_PROC` varchar(200) NOT NULL,
  `RECLAMADA_PROC` varchar(200) NOT NULL,
  `LOCAL_FISICO_PROC` varchar(200) NOT NULL,
  `VALOR_INDENIZACAO_PROC` double NOT NULL,
  `VALOR_EM_RISCO_PROC` double NOT NULL,
  `VALOR_PAGO_PROC` double NOT NULL,
  `DATA_PGTO_PROC` double NOT NULL,
  `PEDIDOS_PROC` varchar(250) NOT NULL,
  `ID_PROC_FK` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_mensagem`
--

CREATE TABLE `tbl_mensagem` (
  `ID_MENSAGEM` int(11) NOT NULL,
  `ID_USUARIO_FK` int(11) NOT NULL,
  `ID_PROC_FK` int(11) NOT NULL,
  `MENS_TITULO` varchar(255) NOT NULL,
  `MENS_DESCRICAO` text NOT NULL,
  `MENS_DATA` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tbl_mensagem`
--

INSERT INTO `tbl_mensagem` (`ID_MENSAGEM`, `ID_USUARIO_FK`, `ID_PROC_FK`, `MENS_TITULO`, `MENS_DESCRICAO`, `MENS_DATA`) VALUES
(19, 39, 13, 'Teste', '			Gshsh', '2018-10-04 11:39:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_processo`
--

CREATE TABLE `tbl_processo` (
  `ID_PROC` int(11) NOT NULL,
  `NOME_PROC` varchar(255) NOT NULL,
  `TIPO_PROC` varchar(50) NOT NULL,
  `NUMERO_PROC` varchar(50) NOT NULL,
  `STATUS_PROC` varchar(50) NOT NULL,
  `SITUACAO_ATUAL_PROC` varchar(255) NOT NULL,
  `RECLAMANTE_PROC` varchar(255) NOT NULL,
  `RECLAMADA_PROC` varchar(255) NOT NULL,
  `LOCAL_PROC` varchar(255) NOT NULL,
  `VALOR_INDENIZACAO_PROC` double NOT NULL,
  `VALOR_RISCO_PROC` double NOT NULL,
  `VALOR_PAGO_PROC` double NOT NULL,
  `DATA_PGTO_PROC` date NOT NULL,
  `PEDIDOS_PROC` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tbl_processo`
--

INSERT INTO `tbl_processo` (`ID_PROC`, `NOME_PROC`, `TIPO_PROC`, `NUMERO_PROC`, `STATUS_PROC`, `SITUACAO_ATUAL_PROC`, `RECLAMANTE_PROC`, `RECLAMADA_PROC`, `LOCAL_PROC`, `VALOR_INDENIZACAO_PROC`, `VALOR_RISCO_PROC`, `VALOR_PAGO_PROC`, `DATA_PGTO_PROC`, `PEDIDOS_PROC`) VALUES
(13, 'TESTE', 'Civel', '4545465', 'Pago', 'TESTETESTETESTE', 'TESTE', 'TESTE', 'TESTE', 3511351, 32132132, 13213212, '1997-11-10', 'TESTETESTETESTETESTETESTE');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_usuario`
--

CREATE TABLE `tbl_usuario` (
  `ID_USER` int(11) NOT NULL,
  `NOME_USER` varchar(255) NOT NULL,
  `LOGON_USER` varchar(255) NOT NULL,
  `SENHA_USER` varchar(255) NOT NULL,
  `CONFIRMA_SENHA_USER` varchar(255) NOT NULL,
  `ADMIN_USER` bit(1) NOT NULL DEFAULT b'0',
  `DELETE_USER` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tbl_usuario`
--

INSERT INTO `tbl_usuario` (`ID_USER`, `NOME_USER`, `LOGON_USER`, `SENHA_USER`, `CONFIRMA_SENHA_USER`, `ADMIN_USER`, `DELETE_USER`) VALUES
(25, 'teste', 'teset', 'teste', 'teste', b'1', b'1'),
(26, 'FRANCIELINE MARQUES DE OLIVEIRA', 'joselucas', 'teste', 'teste', b'0', b'1'),
(27, 'dsadsa', 'dsadsa', 'dsadsa', 'dsadsa', b'0', b'1'),
(31, 'Testando', 'teste', 'teste', 'teste', b'0', b'0'),
(32, 'simone', 'simone', 'simone', 'simone', b'0', b'1'),
(33, 'joao', 'joao', 'joao', 'joao', b'0', b'1'),
(34, 'DSADAS', 'FGD', 'TESTE', 'TESTE', b'0', b'1'),
(37, 'tes', 'te', 'dsa', 'dsa', b'0', b'0'),
(38, 'usuario', 'usuario', 'usuario', 'usuario', b'0', b'0'),
(39, 'admin', 'admin', 'admin', 'admin', b'1', b'1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_advogado`
--
ALTER TABLE `tbl_advogado`
  ADD PRIMARY KEY (`ID_ADV`),
  ADD UNIQUE KEY `ID_USER_FK` (`ID_USER_FK`),
  ADD UNIQUE KEY `OAB_REG_ADV` (`OAB_REG_ADV`),
  ADD KEY `FK_USER_ID` (`ID_ADV`);

--
-- Indexes for table `tbl_advogado_processo`
--
ALTER TABLE `tbl_advogado_processo`
  ADD PRIMARY KEY (`ID_ADV`,`ID_PROC`),
  ADD KEY `FK_PROC_ID` (`ID_PROC`);

--
-- Indexes for table `tbl_detalhe_processo`
--
ALTER TABLE `tbl_detalhe_processo`
  ADD PRIMARY KEY (`ID_DET_PROC`),
  ADD KEY `ID_PROC_FK` (`ID_PROC_FK`);

--
-- Indexes for table `tbl_mensagem`
--
ALTER TABLE `tbl_mensagem`
  ADD PRIMARY KEY (`ID_MENSAGEM`),
  ADD KEY `FK_ID_USUARIO` (`ID_USUARIO_FK`),
  ADD KEY `FK_PROC_ID` (`ID_PROC_FK`);

--
-- Indexes for table `tbl_processo`
--
ALTER TABLE `tbl_processo`
  ADD PRIMARY KEY (`ID_PROC`),
  ADD UNIQUE KEY `NUMERO_PROC` (`NUMERO_PROC`);

--
-- Indexes for table `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  ADD PRIMARY KEY (`ID_USER`),
  ADD UNIQUE KEY `LOGON_USER` (`LOGON_USER`),
  ADD UNIQUE KEY `NOME_USER` (`NOME_USER`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_advogado`
--
ALTER TABLE `tbl_advogado`
  MODIFY `ID_ADV` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_mensagem`
--
ALTER TABLE `tbl_mensagem`
  MODIFY `ID_MENSAGEM` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tbl_processo`
--
ALTER TABLE `tbl_processo`
  MODIFY `ID_PROC` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  MODIFY `ID_USER` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tbl_advogado`
--
ALTER TABLE `tbl_advogado`
  ADD CONSTRAINT `FK_ID_USER` FOREIGN KEY (`ID_USER_FK`) REFERENCES `tbl_usuario` (`ID_USER`);

--
-- Limitadores para a tabela `tbl_advogado_processo`
--
ALTER TABLE `tbl_advogado_processo`
  ADD CONSTRAINT `FK_ADV_ID` FOREIGN KEY (`ID_ADV`) REFERENCES `tbl_advogado` (`ID_ADV`),
  ADD CONSTRAINT `FK_PROC_ID` FOREIGN KEY (`ID_PROC`) REFERENCES `tbl_processo` (`ID_PROC`);

--
-- Limitadores para a tabela `tbl_mensagem`
--
ALTER TABLE `tbl_mensagem`
  ADD CONSTRAINT `FK_ID_PROC` FOREIGN KEY (`ID_PROC_FK`) REFERENCES `tbl_processo` (`ID_PROC`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_ID_USUARIO` FOREIGN KEY (`ID_USUARIO_FK`) REFERENCES `tbl_usuario` (`ID_USER`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
