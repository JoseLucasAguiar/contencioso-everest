
<?php
	include_once("Administracao\ProcessoController.php");
	$protocoloController = new ProcessoController();

	$id = $_GET['id'];
	
	$processo = $protocoloController->selecionaProcessoPeloProtocolo($id);
	$dados = $protocoloController->selecionaMensagensProcessoId($id);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Editar Processo</title>
	<link rel="stylesheet" type="text/css" href="Administracao\css\ProcessoStyle.css">
	<link rel="stylesheet" type="text/css" href="Administracao\css\bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="Administracao\css\EditarProcesso.css">
	<meta charset="UTF-8">
	<meta name="description" content="Free Web tutorials">
	<meta name="keywords" content="HTML,CSS,XML,JavaScript">
	<meta name="author" content="John Doe">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="Administracao\js\Validacoes.js"></script>
</head>
<body class="corpo">

<nav class="navbar navbar-expand-lg"  id="nave">
  <a class="navbar-brand" href="GerenciarProcessos.php">Everest Engenharia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  
  <div>
    <a href="Logout.php"><button class="btn btn-info">Logout</button></a>
  </div>
</nav>
	
	<div class="cabecalho">
        <div class="col-md-6 px-0">
          <h1 class="display-4 font-italic">Edição do processo</h1>
				</div>
					<?php foreach($processo as $p) {?>
						

						<!-- FIRST ROW -->
						<div class="form-row ">

							<div class="col-md-6 bordas">
								<label for id="nome_proc">Nome do Processo: </label>
								<label><?=$p['NOME_PROC']?></label>
							</div>	

							<div class="col-md-6 bordas">
								<label for="situacao_atual">Situação Atual:</label>
								<label><?=$p['SITUACAO_ATUAL_PROC']?></label>	
							</div>
						</div>
						<!-- SECOND ROW -->
						<div class="form-row">
							
							<div class="col-md-2 bordas">
								<label for id="tipo_proc">Tipo do Processo: </label>
								<label id="tipo_proc"><?=$p['TIPO_PROC']?></label>
							</div>

							<div class="col-md-2 bordas">
								<label for id="numero_proc">Número do Processo: </label>
								<label id="numero_proc"><?=$p['NUMERO_PROC']?></label>
							</div>

							<div class="col-md-2 bordas">
								<label id="status_proc">Status do Processo: </label>
								<label for="status_proc"><?=$p['STATUS_PROC']?></label>
							</div>
							<div class="col-md-2 bordas">
								<label for="valor_ind_proc">Valor da Indenização: </label>
								<label><?=$p['VALOR_INDENIZACAO_PROC']?></label>		
							</div>
							<div class="col-md-2 bordas">
								<label for="valor_risco_proc">Valor em Risco: </label>
								<label><?=$p['VALOR_RISCO_PROC']?></label>
							</div>

							<div class="col-md-2 bordas">
								<label for="valor_pago_proc">Valor Pago: </label>
								<label><?=$p['VALOR_PAGO_PROC']?></label>
							</div>
						</div>
				
						<!-- TERCEIRA ROW -->

						<div class="form-row">
							<div class="col-md-6 bordas">
								<label for="reclamente_proc">Reclamante do Processo: </label>
								<label><?=$p['RECLAMANTE_PROC']?></label>	
							</div>
							<div class="col-md-6 bordas">
								<label for="reclamada_proc">Reclamada do Processo: </label>
								<label><?=$p['RECLAMADA_PROC']?></label>	
							</div>
						</div>

						<!-- QUARTA ROW -->
						
						<div class="form-row">
							<div class="col-md-6 bordas">
								<label for="data_pgto_proc">Data de Pagamento: </label>
								<label><?=$p['DATA_PGTO_PROC']?></label>	
							</div>
							<div class="col-md-6 bordas">
								<label for="local_proc">Local do Processo: </label>
								<label><?=$p['LOCAL_PROC']?></label>	
							</div>
						</div>
						
						<!-- QUINTA ROW -->

						<div class="form-group bordas">
							<label for="pedidos_proc">Pedido: </label>
							<label><?=$p['PEDIDOS_PROC']?></label>	
						</div>

					<?php }; ?>
				
        </div>
      </div>

	<div class="divisoria">

	</div>


	<div class="container">

		<ul class="list-unstyled">
	  		
			<?php if(count($dados) == 0){?>
				<div class="msg-aviso">
					<?php echo "NAO HÁ MENSAGEM CADASTRADA"; }?>
				</div>

			<?php foreach($dados as $row) { ?>
				
				<li class="media bordas-msg">
		    			<div class="media-body">
				      		<h5 class="mt-0 mb-1"><?php echo $row["MENS_TITULO"] ?></h5>
				      		<?php echo $row["MENS_DESCRICAO"] ?>
				    	</div>
				    	<div>
				    		<a href=<?="DeletaMensagemDoProcesso.php?idMensagem=".$row['ID_MENSAGEM']."&"."idProcesso=".$row['ID_PROC_FK']?>><button class="btn btn-danger">Deletar</button></a>
				  		</div>
				  	</li>
	  		<?php ;} ?>
		</ul>

	</div>


<div class="form-msg container">
		<form action="CadastrarMensagem.php" method="POST">

			<div class="form-group">
				<label for="corpo" >Escreva um comentário</label>
			<input type="hidden" name="ID_PROC" value="<?php echo $id?>"/> 

			<label for="msg_titulo">Titulo</label>
			<input type="text" id="msg_titulo" name="MENS_TITULO"/> 
			<label for="msg">Mensagem</label>
			<textarea id="msg" name="MENSAGEM_PROCESSO" rows="500" height="200">
			</textarea>

			<input type="submit" class="btn btn-primary" name="cadastrarMensagem" value="Cadastrar Mensagem"/>

		</form>
</div>


<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


</body>
</html>
