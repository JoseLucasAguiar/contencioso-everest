

<!DOCTYPE html>

<html>

<head>
	<title>Login</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link rel="stylesheet" type="text/css" href="Administracao\css\LoginStyle.css">
	<meta charset="UTF-8">
	<meta name="description" content="Free Web tutorials">
	<meta name="keywords" content="HTML,CSS,XML,JavaScript">
	<meta name="author" content="John Doe">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body id="imageBody">

<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-heading">
				<h2 class="text-center">Login - Usuário</h2>
			</div>
			<hr />
			<div class="modal-body">
				<form action="VerificaLogin.php" method="post" id="Login">
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">
							<span class="glyphicon glyphicon-user"></span>
							</span>
							<input type="text" class="form-control" name="login" placeholder="User Name" />
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon">
							<span class="glyphicon glyphicon-lock"></span>
							</span>
							<input type="password" class="form-control" name="senha" placeholder="Password" />

						</div>

					</div>

					<div class="form-group text-center">
						<button type="submit" class="btn btn-success btn-lg">Login</button>
						<!--<a href="#" class="btn btn-link">forget Password</a>-->
					</div>

				</form>
			</div>
		</div>
	</div>


<!--
<body id="LoginForm">
<div class="container">
<h1 class="form-heading">login Form</h1>
<div class="login-form">
<div class="main-div">
   <div class="panel">
   	<h2>Usuário Login</h2>
   	<p>Por favor entre com seu Login e Senha</p>
   </div>
    
    <form action="VerificaLogin.php" method="post" id="Login">
        <div class="form-group">
            <input type="text" class="form-control" id="inputEmail" name="login" placeholder="Login" required>
        </div>
        <div class="form-group">
            <input type="password" class="form-control" id="inputPassword" name="senha" placeholder="Senha" required>
        </div>
		
		<div class="forgot">
        <a href="reset.html">Forgot password?</a>
		</div>
		
        <button type="submit" class="btn btn-primary">Login</button>

    </form>

</div>
<p class="botto-text">EVEREST ENGENHARIA DE INFRAESTRUTURA LTDA</p>
</div>
</div>


-->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</body>




</html>
