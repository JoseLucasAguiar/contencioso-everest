<?php 
// session_start inicia a sessão
include_once("Administracao\UsuarioController.php");
include_once("Administracao\Usuario.php");

$usuario = new Usuario();
$usuarioController = new UsuarioController();

session_start();
// as variáveis login e senha recebem os dados digitados na página anterior
$login = $_POST['login'];
$senha = $_POST['senha'];

$usuario->setLogon($login);
$usuario->setSenha($senha);
$consulta = $usuarioController->confirmaLogin($usuario); 

if($consulta == true)
{
	$_SESSION['login-user'] = $login;
	$_SESSION['senha-user'] = $senha;
	header('location:GerenciarProcessos.php');
}
else{
  unset ($_SESSION['login-user']);
  unset ($_SESSION['senha-user']);
  header('location:Login.php');
  }
?>