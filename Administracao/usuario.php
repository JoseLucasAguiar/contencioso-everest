<?php
	class Usuario{
		private $id;
		private $nome;
		private $logon;
		private $senha;
		private $confirmaSenha;
		private $isAdmin;

		public function setIsAdmin($isAdmin){
			$this->isAdmin = $isAdmin;
		}

		public function setId($id){
			$this->id = $id;
		}

		public function setNome($nome){
			$this->nome = $nome;
		}

		public function setLogon($logon){
			$this->logon = $logon;
		}

		public function setSenha($senha){
			$this->senha = $senha;
		}

		public function setConfirmaSenha($confirmaSenha){
			$this->confirmaSenha = $confirmaSenha;
		}

		
		public function getIsAdmin(){
			return $this->isAdmin;
		}

		public function getId(){
			return $this->id;
		}

		public function getNome(){
			return $this->nome;
		}

		public function getLogon(){
			return $this->logon;
		}

		public function getSenha(){
			return $this->senha;
		}

		public function getConfirmaSenha(){
			return $this->confirmaSenha;
		}
	}
?>