<?php

	class Advogado {
		private $usuario;
		private $oab;

		public function getUsuario(){
			return $this->usuario;
		}

		public function getOab(){
			return $this->oab;
		}

		public function setUsuario(Usuario $usuario){
			$this->usuario = $usuario;
		}

		public function setOab($oab){
			$this->oab = $oab;
		}
	}

?>