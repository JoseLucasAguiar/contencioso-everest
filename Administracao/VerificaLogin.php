<?php 
// session_start inicia a sessão
include_once("UsuarioController.php");
include_once("Usuario.php");

$usuario = new Usuario();
$usuarioController = new UsuarioController();

session_start();
// as variáveis login e senha recebem os dados digitados na página anterior
$login = $_POST['login'];
$senha = $_POST['senha'];

$usuario->setLogon($login);
$usuario->setSenha($senha);
$consulta = $usuarioController->confirmaLoginAdministracao($usuario);

var_dump($consulta);

if($consulta == true)
{
	$_SESSION['login'] = $login;
  $_SESSION['senha'] = $senha;
	header('location:GerenciarProcessos.php');
}
else{
  unset ($_SESSION['login']);
  unset ($_SESSION['senha']);
  header('location:Login.php');
  }
?>