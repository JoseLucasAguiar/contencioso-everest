<?php 
	
	
	class Mensagem
	{
		private $id;
		private $titulo;
		private $mensagem;
		private $idUsuarioFK;
		private $idProcFK;

		public function getId(){
			return $this->id;
		}

		public function getTitulo(){
			return $this->titulo;
		}

		public function getMensagem(){
			return $this->mensagem;
		}

		public function getIdUsuarioFK(){
			return $this->idUsuarioFK;
		}

		public function getIdProcFk(){
			return $this->idProcFK;
		}

		public function setId($id){
			$this->id = $id;
		}

		public function setTitulo($titulo){
			$this->titulo = $titulo;
		}

		public function setMensagem($mensagem){
			$this->mensagem = $mensagem;
		}

		public function setIdUsuarioFK($idUsuarioFK){
			$this->idUsuarioFK = $idUsuarioFK;
		}

		public function setIdProcFK($idProcFK){
			$this->idProcFK = $idProcFK;
		}



	}



?>