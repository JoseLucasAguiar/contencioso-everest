
<?php
    class VincularAdvogadoController{

        public function vinculaAdvogadoComProcesso($idProc, $idAdv){
            
            try{
                $pdo = $this->getConnection();
                $sql = $pdo->prepare( "INSERT INTO TBL_ADVOGADO_PROCESSO(ID_PROC, ID_ADV) VALUES(:idProc, :idAdv)");
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $sql->bindValue(":idProc", $idProc);
                $sql->bindValue(":idAdv", $idAdv);
                $sql->execute();
                return true;
            }catch(PDOException $ex){
                return false;
            }
        }


        private function getConnection(){
			
			try{
				$pdo = new PDO('mysql:host=localhost;dbname=sistema', 'root', '');

				return $pdo;
			}
			catch(PDOException $ex){
				echo $ex->getMessage();
			}

			return null;
		} 


    }
?>