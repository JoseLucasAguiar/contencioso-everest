<?php
    include_once("AdvogadoController.php");
    include_once("ProcessoController.php");
    $advogadoController = new AdvogadoController();
    $advogados = $advogadoController->selecionaAdvogados();
    $processoController = new ProcessoController();
    $processos = $processoController->selecionaProcessos();
?>

<html>

<head>
  <?php 
    session_start();
    if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
    {
        unset($_SESSION['login']);
        unset($_SESSION['senha']);
        header('location:Login.php');
        }
    
    $logado = $_SESSION['login'];
  ?>

	<title>Vincular Advogado ao Processo</title>
	<link rel="stylesheet" type="text/css" href="css\bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css\CadastraUsuario.css">
	<meta charset="UTF-8">
	<meta name="description" content="Free Web tutorials">
	<meta name="keywords" content="HTML,CSS,XML,JavaScript">
	<meta name="author" content="John Doe">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="js\Validacoes.js"></script>
</head>

<body>


<nav class="navbar navbar-expand-lg"  id="nave">
  <a class="navbar-brand" href="GerenciarProcessos.php">Everest Engenharia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Gerenciamento
        </a>
        <div class="dropdown-menu" id="dropdown" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="GerenciarUsuarios.php">Usuários</a>
          <a class="dropdown-item" href="GerenciarAdvogados.php">Advogados</a>
          <a class="dropdown-item" href="GerenciarProcessos.php">Processos</a>
        </div>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cadastros
        </a>
        <div class="dropdown-menu" id="dropdown" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="cadastrarProcesso.php">Cadastrar Processo</a>
          <a class="dropdown-item" href="CadastraUsuario.php">Cadastrar Usuário</a>
          <a class="dropdown-item" href="CadastrarAdvogado.php">Cadastrar Advogado</a>
        </div>
      </li>
    </ul>
  </div>
  <div>
    <a href="Logout.php"><button class="btn btn-info">Logout</button></a>
  </div>
</nav>
<div class="alert alert-danger" role="alert" id="msg-erro">
</div>

<div class="corpo">

<form action="ValidaVinculoProcesso.php" method="post" id="vinculo_adv_proc"> 
  
<div class="form-row form-group">
    <div class="col-md-3"> </div>
    <div class="col-md-6">
      <label for="processo_id" >Selecione o processo: </label>
      <select id="processo_id" required name="ID_PROC">
            <?php foreach($processos as $row) { ?>
                    <option value="<?=$row["ID_PROC"]?>"> <?=$row["NOME_PROC"]?> </option>
            <?php  } ?>
      </select>
    </div>
  </div>
  
  <div class="form-row form-group">
    <div class="col-md-3"> </div>
    <div class="col-md-6">
      <label for="advogado_id" >Selecione o advogado: </label>
      <select id="advogado_id" required name="ID_ADV">
            <?php foreach($advogados as $row) { ?>
                    <option value="<?=$row["ID_ADV"]?>"> <?=$row["NOME_USER"]?> </option>
            <?php  } ?>
      </select>
    </div>
  </div>

  <div class="form-row">
    <div class="col-md-5 "></div>
    <input type="submit" class="btn btn-success" class="form-control" name="Cadastrar Advogado" value="Cadastrar Advogado">
  </div>
</form>
            
<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</div>

</body>
</html>