<?php

	include_once("Advogado.php");

	class AdvogadoController{

		public function insereAdvogado(Advogado $advogado){
			try{

				$pdo = $this->getConnection();

				$sql = $pdo->prepare("INSERT INTO TBL_ADVOGADO (OAB_REG_ADV, ID_USER_FK) 
    VALUES (:oab,:user_i)");

				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->bindValue(":user_i",$advogado->getUsuario()->getId());
				$sql->bindValue(":oab",$advogado->getOab());
				$sql->execute();

			}catch(PDOException $ex){
				echo $ex->getMessage();
			}
		}

		public function isAdvogadoValido($idAdvogado){
			try	{
				$pdo = $this->getConnection();
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql = $pdo->prepare("SELECT ID_ADV FROM TBL_ADVOGADO WHERE ID_ADV = :id");
				$sql->bindValue(":id", $idAdvogado);
				$sql->execute();

				if( count($sql->fetchAll()) == 0 )
					return false;
				return true;
			}catch(PDOException $ex){
				echo $ex->getMessage();
			}
		}

		public function selecionaAdvogados(){
			try{
				$pdo = $this->getConnection();

				$sql = $pdo->prepare("SELECT AD.ID_ADV, AD.OAB_REG_ADV, AD.ID_USER_FK, US.NOME_USER,  US.ADMIN_USER FROM TBL_ADVOGADO AS AD INNER JOIN TBL_USUARIO AS US ON AD.ID_USER_FK = US.ID_USER");
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->execute();

				return $sql->fetchAll(); 

			}catch(PDOException $ex){
				echo $ex->getMessage();
			}

		}


		private function getConnection(){
			
			try{
				$pdo = new PDO('mysql:host=localhost;dbname=sistema', 'root', '');

				return $pdo;
			}
			catch(PDOException $ex){
				echo $ex->getMessage();
			}

			return null;
		} 


	}



?>