<?php
    include_once("ProcessoController.php");
    include_once("AdvogadoController.php");
    include_once("VincularAdvogadoController.php");

    $processoController = new ProcessoController();
    $advogadoController = new AdvogadoController();
    $vincularController = new VincularAdvogadoController();

    $idProcesso = $_POST['ID_PROC'];
    $idAdvogado = $_POST['ID_ADV'];

    if( $processoController->isProcessoValido($idProcesso) && $advogadoController->isAdvogadoValido($idAdvogado) ){
        if($vincularController->vinculaAdvogadoComProcesso($idProcesso, $idAdvogado))
            echo "Deu certo a vinculacao";
        else
            echo "Vinculacao deu errado";
    }else{
        echo "Deu errado";
    }
?>