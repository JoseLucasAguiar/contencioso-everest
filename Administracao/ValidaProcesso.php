<?php  

	include_once("Processo.php");
	include_once("ProcessoController.php");

	$processo = new Processo();
	$processoController = new ProcessoController();

	$nomeProc = $_POST['nome_proc'];
	$protocoloProc = $_POST['protocolo_proc'];
	$tipoProc = $_POST['tipo_proc'];
	$statusProc = $_POST['status_proc'];
	$situacaoAtual = $_POST['situacao_atual'];

	$reclamante = $_POST['reclamante_proc'];
	$reclamada = $_POST['reclamado_proc'];
	$localFisico = $_POST['local_proc'];
	$indenizacao = $_POST['indenizacao_proc'];
	$risco = $_POST['valor_risco'];
	$pago = $_POST['valor_pago'];
	$dataPgto = $_POST['data_pgto'];
	$pedidos = $_POST['pedidos_proc'];

	$processo->setNome($nomeProc);
	$processo->setNumero($protocoloProc);
	$processo->setTipo($tipoProc);
	$processo->setStatus($statusProc);
	$processo->setSituacaoAtual($situacaoAtual);

	$processo->setReclamante($reclamante);
	$processo->setReclamada($reclamada);
	$processo->setLocalFisico($localFisico);
	$processo->setValorIndenizacao($indenizacao);
	$processo->setValorEmRisco($risco);
	$processo->setValorPago($pago);
	$processo->setDataPagamento($dataPgto);
	$processo->setPedidos($pedidos);

	$processoController->insereProcesso($processo);
	header("location: cadastrarProcesso.php");
?>