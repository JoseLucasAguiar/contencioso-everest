

<?php
	include_once("ProcessoController.php");
	$protocoloController = new ProcessoController();

	$id = $_GET['id'];
	
	$dados = $protocoloController->selecionaMensagensProcessoId($id);//selecionaProcessos();//selecionaProcessoPeloProtocolo($id);
	//var_dump($dados);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Detalhes do Processo</title>
	<link rel="stylesheet" type="text/css" href="css\ProcessoStyle.css">
	<link rel="stylesheet" type="text/css" href="css\bootstrap.min.css">
	<meta charset="UTF-8">
	<meta name="description" content="Free Web tutorials">
	<meta name="keywords" content="HTML,CSS,XML,JavaScript">
	<meta name="author" content="John Doe">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Everest Engenharia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="GerenciarProcessos.php">Gerênciar Processos<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cadastros
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="cadastrarProcesso.html">Cadastrar Processo</a>
          <a class="dropdown-item" href="CadastraUsuario.php">Cadastrar Usuário</a>
          <a class="dropdown-item" href="CadastrarAdvogado.php">Cadastrar Advogado</a>
        </div>
      </li>
    </ul>
  </div>
</nav>

	
	<div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
          <h1 class="display-4 font-italic">Detalhes do Processo</h1>
          <p class="lead my-3"></p>
          <p class="lead mb-0"><a href="#" class="text-white font-weight-bold"></a></p>
        </div>
      </div>

	<div class="container">

	<ul class="list-unstyled">
  		
		<?php foreach($dados as $row) {?>
			<li class="media">
    			<div class="media-body">
		      		<h5 class="mt-0 mb-1"><?php echo $row["MENS_TITULO"] ?></h5>
		      		<?php echo $row["MENS_DESCRICAO"] ?>
		    	</div>
		  	</li>
		  		<hr>
  		<?php ;} ?>
	</ul>


	<form action="CadastrarMensagem.php" method="POST">

		<label for="corpo">Escreva um comentário</label>
		<input type="hidden" name="ID_PROC" value="<?php echo $id?>"/> 
		
		<label for="msg_titulo">Titulo</label>
		<input type="text" id="msg_titulo" name="MENS_TITULO"/> 
		<label for="msg">Mensagem</label>
		<textarea id="msg" name="MENSAGEM_PROCESSO" rows="500" height="200">
		</textarea>
		<input type="submit" class="btn btn-primary" name="cadastrarMensagem" value="Cadastrar Mensagem"/>

	</form>

</div>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


</body>
</html>
