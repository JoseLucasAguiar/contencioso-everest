<?php
	
	class Processo{
		private $id;
		private $nome;
		private $numero;
		private $tipo;
		private $reclamante;
		private $reclamada;
		private $localFisico;
		private $valorIndenizacao;
		private $valorEmRisco;
		private $valorPago;
		private $dataPagamento;
		private $status;
		private $pedidos;
		private $situacaoAtual;

		public function setId($id){
			$this->id = $id;
		}

		public function setNome($nome){
			$this->nome = $nome;
		}

		public function setNumero($numero){
			$this->numero = $numero;
		}

		public function setTipo($tipo){
			$this->tipo = $tipo;
		}

		public function setReclamante($reclamante){
			$this->reclamante = $reclamante;
		}

		public function setReclamada($reclamada){
			$this->reclamada = $reclamada;
		}

		public function setLocalFisico($localFisico){
			$this->localFisico = $localFisico;
		}

		public function setValorIndenizacao($valorIndenizacao){
			$this->valorIndenizacao = $valorIndenizacao;
		}

		public function setValorEmRisco($valorEmRisco){
			$this->valorEmRisco = $valorEmRisco;
		}

		public function setValorPago($valorPago){
			$this->valorPago = $valorPago;
		}

		public function setDataPagamento($dataPagamento){
			$this->dataPagamento = $dataPagamento;
		}

		public function setStatus($status){
			$this->status = $status;
		}

		public function setPedidos($pedidos){
			$this->pedidos = $pedidos;
		}

		public function setSituacaoAtual($situacaoAtual){
			$this->situacaoAtual = $situacaoAtual;
		}

		//GETS
		public function getId(){
			return $this->id;
		}

		public function getNome(){
			return $this->nome;
		}

		public function getNumero(){
			return $this->numero;
		}

		public function getTipo(){
			return $this->tipo;
		}

		public function getReclamante(){
			return $this->reclamante;
		}

		public function getReclamada(){
			return $this->reclamada;
		}

		public function getLocalFisico(){
			return $this->localFisico;
		}

		public function getValorIndenizacao(){
			return $this->valorIndenizacao;
		}

		public function getValorEmRisco(){
			return $this->valorEmRisco;
		}

		public function getValorPago(){
			return $this->valorPago;
		}

		public function getDataPagamento(){
			return $this->dataPagamento;
		}

		public function getStatus(){
			return $this->status;
		}

		public function getPedidos(){
			return $this->pedidos;
		}

		public function getSituacaoAtual(){
			return $this->situacaoAtual;
		}


	}


?>