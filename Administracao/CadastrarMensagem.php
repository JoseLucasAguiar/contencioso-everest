<?php
	session_start();
	if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
	{
	unset($_SESSION['login']);
	unset($_SESSION['senha']);
	header('location:Login.php');
	}
	
	$logado = $_SESSION['login'];
	
	include_once("ProcessoController.php");
	include_once("Mensagem.php");
	include_once("UsuarioController.php");
	include_once("Usuario.php");

	$processoController = new ProcessoController();
	$mensagem = new Mensagem();
	$usuarioController = new UsuarioController();
	$usuario = new Usuario();


	$id = $_POST["ID_PROC"];
	$msg = $_POST["MENSAGEM_PROCESSO"];
	$titulo = $_POST["MENS_TITULO"];
	$usuario = $usuarioController->selecionaIdPeloLogin($logado);
	$mensagem->setIdProcFK($id);
	$mensagem->setMensagem($msg);
	$mensagem->setTitulo($titulo);
	$mensagem->setIdUsuarioFK($usuario[0]);

	$processoController->insereMensagemDoProcesso($mensagem);
	
	header("location: EditarProcesso.php?id=".$id);
?>