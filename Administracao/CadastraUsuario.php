<html>

<head>
    <?php 
  session_start();
  if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
  {
    unset($_SESSION['login']);
    unset($_SESSION['senha']);
    header('location:Login.php');
    }
  
  $logado = $_SESSION['login'];
  ?>

	<title>Cadastro de Usuário</title>
	<link rel="stylesheet" type="text/css" href="css\bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css\CadastraUsuario.css">
	<meta charset="UTF-8">
	<meta name="description" content="Free Web tutorials">
	<meta name="keywords" content="HTML,CSS,XML,JavaScript">
	<meta name="author" content="John Doe">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="js\Validacoes.js"></script>
</head>

<body>


<nav class="navbar navbar-expand-lg"  id="nave">
  <a class="navbar-brand" href="GerenciarProcessos.php">Everest Engenharia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Gerenciamento
        </a>
        <div class="dropdown-menu" id="dropdown" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="GerenciarUsuarios.php">Usuários</a>
          <a class="dropdown-item" href="GerenciarAdvogados.php">Advogados</a>
          <a class="dropdown-item" href="GerenciarProcessos.php">Processos</a>
        </div>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cadastros
        </a>
        <div class="dropdown-menu" id="dropdown" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="cadastrarProcesso.php">Cadastrar Processo</a>
          <a class="dropdown-item" href="CadastraUsuario.php">Cadastrar Usuário</a>
          <a class="dropdown-item" href="CadastrarAdvogado.php">Cadastrar Advogado</a>
        </div>
      </li>
    </ul>
  </div>
  <div>
    <a href="Logout.php"><button class="btn btn-info">Logout</button></a>
  </div>
</nav>
<div class="alert alert-danger" role="alert" id="msg-erro">
</div>

<div class="corpo">
<!--<form action="validar.php" method="post">-->
<form method="post" id="usuario" action="javascript:cadastraUsuario();">
  <div class="form-group">
	  <label for="name_id" class="form-control exibicao">Nome: </label>
	  <input type="text" id="name_id" class="form-control entrada" required placeholder="Digite seu nome" name="nome">
	</div>
  <div class="form-group">
    <label for="login_id" class="form-control exibicao">Nome de Logon: </label>
	  <input type="text" class="form-control entrada" required placeholder="Digite o nome de Logon" name="login">
  </div>
  <div class="form-group">
	  <label for="senha_id" class="form-control exibicao">Senha: </label>
	  <input type="password" id="senha_id" class="form-control entrada" required placeholder="Digite sua senha" name="senha">
	</div>
  <div class="form-group">
	  <label for="senha_confirma_id" class="form-control exibicao">Corfirma Senha: </label>
	  <input type="password" id="senha_confirma_id" class="form-control entrada" required placeholder="Digite a confirmação da senha" name="confirma_senha">
  </div>

  <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="radio" name="perfil_user" id="rd1" value="1" checked>
        <label class="form-check-label" for="rd1">Administrador</label>
    </div>

    <div class="form-check">
      <input class="form-check-input" type="radio" name="perfil_user" id="rd2" value="0">
        <label class="form-check-label" for="rd2">Usuário</label>
    </div>
	</div>
    

	<input type="submit" class="btn btn-success" name="Cadastrar Usuário" value="Cadastrar Usuário">
</form>

            
<!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</div>

</body>
</html>