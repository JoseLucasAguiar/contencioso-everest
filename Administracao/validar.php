<?php
	include_once('UsuarioController.php');
	include_once('usuario.php');

	$usuario = new Usuario();
	$db = new UsuarioController();

	$nome = $_POST['nome'];
	$login = $_POST['login'];
	$senha = $_POST['senha'];
	$confirmaSenha = $_POST['confirma_senha'];
	$perfilUser = $_POST['perfil_user'];

	if($perfilUser == 0)
		$usuario->setIsAdmin(false);
	else
		$usuario->setIsAdmin(true);


	$usuario->setNome($nome);
	$usuario->setLogon($login);
	$usuario->setSenha($senha);
	$usuario->setConfirmaSenha($confirmaSenha);
	

	$send = ($db->insereUsuarios($usuario));
	
	if( $send ){
		$response_array['status'] = 'success';  
	} else {
		$response_array['status'] = 'error';  
	}
	
	echo json_encode( $response_array );
?>