<?php


	class UsuarioController{

		//$senha = '123456';
	//$senha = md5($senha);
	//echo $senha;	
		
		public function confirmaLogin(Usuario $usuario){
			try{
				$pdo = $this->getConnection();

				$sql = $pdo->prepare("SELECT * FROM TBL_USUARIO WHERE LOGON_USER = :login AND SENHA_USER = :senha AND ADMIN_USER = 0");

				$sql->bindValue(":login", $usuario->getLogon());
				$sql->bindValue(":senha", md5($usuario->getSenha()));
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->execute();

				return $sql->fetchAll();
			}catch(PDOException $ex){
				echo $ex->getMessage();
			}
		}
		
		public function confirmaLoginAdministracao(Usuario $usuario){
			try{
				$pdo = $this->getConnection();

				$sql = $pdo->prepare("SELECT * FROM TBL_USUARIO WHERE LOGON_USER = :login AND SENHA_USER = :senha AND ADMIN_USER = 1");

				$sql->bindValue(":login", $usuario->getLogon());
				$sql->bindValue(":senha", md5($usuario->getSenha()));
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->execute();

				return $sql->fetchAll();
			}catch(PDOException $ex){
				echo $ex->getMessage();
			}
		}

		public function insereUsuarios(Usuario $usuario){

			try{

				$pdo = $this->getConnection();

				$sql = $pdo->prepare("INSERT INTO TBL_USUARIO (NOME_USER, LOGON_USER, SENHA_USER, CONFIRMA_SENHA_USER, ADMIN_USER) 
    VALUES (:nome,:logon,:senha,:confirma_senha,:is_admin)");

				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				$sql->bindValue(":nome",$usuario->getNome());
				$sql->bindValue(":logon",$usuario->getLogon());
				$sql->bindValue(":senha",md5($usuario->getSenha()));
				$sql->bindValue(":confirma_senha",md5($usuario->getConfirmaSenha()));
				$sql->bindValue(":is_admin",$usuario->getIsAdmin());
				
				$sql->execute();

				//header('Location:login.php');

			}catch(PDOException $ex){
				echo $ex->getMessage();
			}

		}

		public function selecionaUsuariosCompleto(){

			try{

				$pdo = $this->getConnection();

				$sql = $pdo->prepare("SELECT US.DELETE_USER, AD.ID_ADV, CASE WHEN AD.OAB_REG_ADV IS NULL THEN 'NÃO É ADVOGADO' ELSE AD.OAB_REG_ADV END AS OAB_REG_ADV , US.ID_USER, US.LOGON_USER, US.NOME_USER, CASE WHEN US.ADMIN_USER = 1 THEN 'ADMINISTRADOR' ELSE 'USUÁRIO' END AS PERFIL_USER FROM TBL_USUARIO AS US LEFT JOIN TBL_ADVOGADO AS AD ON AD.ID_USER_FK = US.ID_USER ORDER BY US.NOME_USER");

				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->execute();

				return $sql->fetchAll(); 

			}catch(PDOException $ex){
				echo $ex->getMessage();
			}
			return null;
		}

		public function selecionaUsuarios(){

			try{

				$pdo = $this->getConnection();
				// SELECIONA SOMENTE OS USUÁRIOS QUE NÃO SAO ADVOGADOS
				$sql = $pdo->prepare("SELECT * FROM TBL_USUARIO LEFT JOIN TBL_ADVOGADO ON ID_USER_FK = ID_USER WHERE ID_USER_FK IS NULL AND DELETE_USER = 0 ORDER BY NOME_USER");

				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->execute();
				return $sql->fetchAll();

			}catch(PDOException $ex){
				echo $ex->getMessage();
			}
			return null;
		}

		public function selecionaIdPeloLogin($login){
			
			try{
				$pdo = $this->getConnection();
				$sql = $pdo->prepare("SELECT ID_USER FROM TBL_USUARIO WHERE LOGON_USER = :logon");
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->bindValue(":logon",$login);
				$sql->execute();
				
				return $sql->fetch(); 

			}catch(PDOException $ex){
				echo $ex->getMessage();
			}
			return null;
		}



		public function selecionaUsuarioPeloNome($nome){

			try{

				$pdo = $this->getConnection();
				$sql = $pdo->prepare("SELECT * FROM TBL_USUARIO WHERE NOME_USER = :nome");
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->bindValue(":nome",$nome);
				$sql->execute();

				return $sql->fetchAll(); 

			}catch(PDOException $ex){
				echo $ex->getMessage();
			}
			return null;
		}

		public function deletaUsuario(Usuario $usuario){
			try{
				print_r($usuario->getId());
				$pdo = $this->getConnection();
				$sql = $pdo->prepare("UPDATE TBL_USUARIO SET DELETE_USER = 1 WHERE ID_USER = :id");

				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->bindValue(":id", $usuario->getId());
				$sql->execute();
			    return true;
			}catch(PDOException $ex){
				echo $ex->getMessage();
				return false;
			}
		}

		private function getConnection(){
			
			try{
				$pdo = new PDO('mysql:host=localhost;dbname=sistema', 'root', '');

				return $pdo;
			}
			catch(PDOException $ex){
				echo $ex->getMessage();
			}

			return null;
		} 

		

	}

?>