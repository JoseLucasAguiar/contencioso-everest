

<html>

<head>
	 <?php 
    session_start();
    if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
    {
      unset($_SESSION['login']);
      unset($_SESSION['senha']);
      header('location:Login.php');
    }
  ?>

	<title>Cadastrar Processo</title>
	<link rel="stylesheet" type="text/css" href="css\ProcessoStyle.css">
	<link rel="stylesheet" type="text/css" href="css\bootstrap.min.css">
	<meta charset="UTF-8">
  <meta name="description" content="Free Web tutorials">
  <meta name="keywords" content="HTML,CSS,XML,JavaScript">
  <meta name="author" content="John Doe">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>

<body class="corpo">

<nav class="navbar navbar-expand-lg"  id="nave">
  <a class="navbar-brand" href="GerenciarProcessos.php">Everest Engenharia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Gerenciamento
        </a>
        <div class="dropdown-menu" id="dropdown" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="GerenciarUsuarios.php">Usuários</a>
          <a class="dropdown-item" href="GerenciarAdvogados.php">Advogados</a>
          <a class="dropdown-item" href="GerenciarProcessos.php">Processos</a>
        </div>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cadastros
        </a>
        <div class="dropdown-menu" id="dropdown" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="cadastrarProcesso.php">Cadastrar Processo</a>
          <a class="dropdown-item" href="CadastraUsuario.php">Cadastrar Usuário</a>
          <a class="dropdown-item" href="CadastrarAdvogado.php">Cadastrar Advogado</a>
        </div>
      </li>
    </ul>
  </div>
  <div>
    <a href="Logout.php"><button class="btn btn-info">Logout</button></a>
  </div>
</nav>


<div class="container">

	<div id="margens">

<form action="ValidaProcesso.php" method="POST">
	
<div class="form-row">
	<div class="form-group col-md-6">
		<label for="nome_proc" >Nome do Processo: </label>
		<input type="text" required name="nome_proc" class="form-control" id="nome_proc" placeholder="Nome do Processo" />
	</div>
	
	<div class="form-group col-md-6">
		<label for="protocolo_proc">Protocolo do Processo: </label>
		<input type="text" required id="protocolo_proc" name="protocolo_proc" placeholder="Protocolo do Processo" />
	</div>
</div>

<div class="form-group">
		<label for="reclamante_proc">Reclamante: </label>
		<input type="text" required id="reclamante_proc" name="reclamante_proc" class="form-control" placeholder="Reclamante do Processo" />
</div>
<div class="form-group">
		<label for="reclamado_proc">Reclamado: </label>
		<input type="text" required id="reclamado_proc" name="reclamado_proc" placeholder="Reclamado do Processo" />
</div>

<div class="form-row">
	<div class="col-md-6">
		<label for="local_proc">Local: </label>
		<input type="text" id="local_proc" name="local_proc" class="form-control" placeholder="Local" />
	</div>
	<div class="col-md-6">
			<label for="indenizacao_proc">Valor da Indenização: </label>
			<input type="text" id="indenizacao_proc" name="indenizacao_proc" placeholder="Valor da indenização" />
	</div>
</div>


<div class="form-row">
	<div class="col-md-6">
		<label for="valor_risco">Valor em Risco: </label>
		<input type="text" id="valor_risco" name="valor_risco" placeholder="Valor em Risco" />
	</div>
	<div class="col-md-6">
			<label for="valor_pago">Valor Pago: </label>
			<input type="number" id="valor_pago" name="valor_pago"placeholder="0,00" min="0" step="0.01"/>
	</div>
</div>

<div class="form-row">
	<div class="col-md-4">
		<label for="data_pgto">Data de pagamento: </label>
		<input type="date" id="data_pgto" name="data_pgto" placeholder="Data de Pagamento" />
	</div>

	<div class="col-md-4">
			<label for="tipo_proc">Tipo do Processo: </label>
			<select id="tipo_proc" name="tipo_proc">
					<option value="Civel">Civel</option>
					<option value="Execução Fiscal">Execução Fiscal</option>
					<option value="Indenização">Indenização</option>
					<option value="Pregão">Pregão</option>
					<option value="Trabalhista">Trabalhista</option>
					<option value="Tributário">Tributário</option>
					<option value="Outros">Outros</option>
				</select>
	</div>

	<div class="col-md-4">
			<label for="status_proc">Status do Processo</label>
			<select id="status_proc" name="status_proc">
				<option value="Pago">Pago</option>
				<option value="Em andamento">Em andamento</option>
				<option value="Iniciado">Iniciado</option>
			</select>
	</div>
	
</div>

<div class="form-group">
		<label for="situacao_atual">Situação Atual: </label>
		<textarea id="situacao_atual" name="situacao_atual" rows="10" cols="60"></textarea>
</div>
<div class="form-group">
		<label for="pedidos_proc">Pedidos do Processo: </label>
		<textarea id="pedidos_proc" name="pedidos_proc" rows="10" cols="60"></textarea>
</div>
	<input class="btn btn-success" type="submit" name="Cadastrar Processo" value="Cadastrar Processo">
</form>

</div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
</html>