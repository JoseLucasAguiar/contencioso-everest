<?php
	include_once('UsuarioController.php');
	include_once('AdvogadoController.php');
	include_once('usuario.php');
	include_once('Advogado.php');

	$usuario = new Usuario();
	$advogado = new Advogado();
	$usuarioController = new UsuarioController();
	$advogadoController = new AdvogadoController();
	
	$registro_oab = $_POST['registro_adv'];
	$nome_user = $_POST['usuario_nome'];

	$dados = $usuarioController->selecionaUsuarioPeloNome($nome_user);
	
	$usuario->setId($dados[0]['ID_USER']);
	$usuario->setNome($dados[0]["NOME_USER"]);
	$usuario->setLogon($dados[0]["LOGON_USER"]);
	$usuario->setSenha($dados[0]["SENHA_USER"]);
	$usuario->setConfirmaSenha($dados[0]["CONFIRMA_SENHA_USER"]);
	
	$advogado->setOab($registro_oab);
	$advogado->setUsuario($usuario);

	$send = ($advogadoController->insereAdvogado($advogado));
	
	if( $send ){
		$response_array['status'] = 'success';  
	} else {
		$response_array['status'] = 'error';  
	}
	
	echo json_encode( $response_array );
?>