<?php 
    session_start();
    if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
    {
      unset($_SESSION['login']);
      unset($_SESSION['senha']);
      header('location:Login.php');
    }
	
	include_once("ProcessoController.php");
	include_once("Processo.php");

	$processoController = new ProcessoController();
	$processo = new Processo();

	$id = $_GET['id'];

	$processo->setId($id);

	if($processoController->deletaProcesso($processo)){
		echo "Processo deletado com sucesso";
		header("location: GerenciarProcessos.php");
	}
	else{
		echo "Falha ao deletar processo";
	}

?>