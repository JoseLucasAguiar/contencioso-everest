
<?php
	include_once("UsuarioController.php");
	$usuarioController = new UsuarioController();
  $dados = $usuarioController->selecionaUsuariosCompleto();
?>
<html>

<head>

  <?php 
    session_start();
    if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
    {
      unset($_SESSION['login']);
      unset($_SESSION['senha']);
      header('location:Login.php');
      }
  ?>

	<title>Gerenciamento de Usuários</title>
	<link rel="stylesheet" type="text/css" href="css\ProcessoStyle.css">
	<link rel="stylesheet" type="text/css" href="css\bootstrap.min.css">
	<meta charset="UTF-8">
	<meta name="description" content="Free Web tutorials">
	<meta name="keywords" content="HTML,CSS,XML,JavaScript">
	<meta name="author" content="John Doe">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script
  src="https://code.jquery.com/jquery-3.3.1.slim.js"
  integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
  crossorigin="anonymous"></script>
<script src="Login-Script.js" type="text/javascript">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>

<body id="content">


<nav class="navbar navbar-expand-lg"  id="nave">
  <a class="navbar-brand" href="GerenciarProcessos.php">Everest Engenharia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Gerenciamento
        </a>
        <div class="dropdown-menu" id="dropdown" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="GerenciarUsuarios.php">Usuários</a>
          <a class="dropdown-item" href="GerenciarAdvogados.php">Advogados</a>
          <a class="dropdown-item" href="GerenciarProcessos.php">Processos</a>
        </div>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cadastros
        </a>
        <div class="dropdown-menu" id="dropdown" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="cadastrarProcesso.php">Cadastrar Processo</a>
          <a class="dropdown-item" href="CadastraUsuario.php">Cadastrar Usuário</a>
          <a class="dropdown-item" href="CadastrarAdvogado.php">Cadastrar Advogado</a>
        </div>
      </li>
    </ul>
  </div>
  <div>
    <a href="Logout.php"><button class="btn btn-info">Logout</button></a>
  </div>
</nav>

<div id="margens">
<!--<input class="form-control" id="myInput" type="text" placeholder="Search..">-->

<div style="overflow-x:auto;">
  <table class="table">
    <tr>
      <th>Ativo</th>
      <th>Nome</th>
      <th>Login</th>
      <th>OAB</th>
      <th>Perfil</th>
      <!--<th>Editar</th>-->
      <th>Deletar</th>
   
    </tr>
    <tbody id="tabela">
    <?php foreach($dados as $row) { ?>
    
    <tr>
      <?php if($row['DELETE_USER'] == 0) {?>
        <td><img src="img\ic_positive.png" alt="Ativo" width="22px" heigth="22px"></img></td>
      <?php } else {?>
        <td><img src="img\ic_negative.png" alt="Não Ativo" width="22px" heigth="22px" ></img></td>
      <?php }?>
      <td value="<?=$row["NOME_USER"]?>"> <?=$row["NOME_USER"]?></td>
      <td value="<?=$row["LOGON_USER"]?>"> <?=$row["LOGON_USER"]?></td>
      <td value="<?=$row["OAB_REG_ADV"]?>"> <?=$row["OAB_REG_ADV"]?></td>  
      <td value="<?=$row["PERFIL_USER"]?>"> <?=$row["PERFIL_USER"]?></td>  
      <!--<td><a href="<?="EditarUsuario.php?id=".$row['ID_USER'] ?>" > <button class="btn btn-primary">Editar</button></a></td>-->
      <td><a href="<?="DeletaUsuario.php?id=".$row['ID_USER'] ?>" ><button class="btn btn-warning">Excluir</button></a></td>
    </tr>
    
    <?php ;} ?>
    </tbody>
  </table>
</div>

    

</div>

</body>
</html>