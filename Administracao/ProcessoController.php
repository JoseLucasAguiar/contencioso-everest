<?php 
	
	include_once("Processo.php");

	class ProcessoController{

		public function selecionaProcessoPeloProtocolo($id){

			try{

				$pdo = $this->getConnection();
				$sql = $pdo->prepare("SELECT * FROM TBL_PROCESSO WHERE ID_PROC = :numero");
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->bindValue(":numero",$id);
				$sql->execute();

				return $sql->fetchAll(); 

			}catch(PDOException $ex){
				echo $ex->getMessage();
			}
			return null;
		}

		public function isProcessoValido($idProcesso){
			try	{
				$pdo = $this->getConnection();
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql = $pdo->prepare("SELECT ID_PROC FROM TBL_PROCESSO WHERE ID_PROC = :id");
				$sql->bindValue(":id", $idProcesso);
				$sql->execute();

				if( count($sql->fetchAll()) == 0 )
					return false;
				return true;
			}catch(PDOException $ex){
				echo $ex->getMessage();
			}
		}

		
		public function selecionaProcessos(){

			try{

				$pdo = $this->getConnection();

				$sql = $pdo->prepare("SELECT * FROM TBL_PROCESSO");

				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->execute();

				return $sql->fetchAll(); 

			}catch(PDOException $ex){
				echo $ex->getMessage();
			}
			return null;
		}

		public function selecionaMensagensProcessoId($id){
			try{
				$pdo = $this->getConnection();
				$sql = $pdo->prepare("SELECT * FROM TBL_MENSAGEM WHERE ID_PROC_FK = :id");

				$sql->bindValue(":id", $id);

				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->execute();

				return $sql->fetchAll();
			}catch(PDOException $ex){
				echo $ex->getMessage();
			}

			return null;
		}

		public function insereMensagemDoProcesso(Mensagem $mensagem){
			try{
				$pdo = $this->getConnection();
				//$sql = $pdo->prepare("INSERT INTO TBL_MENSAGEM(ID_PROC_FK, MENS_DESCRICAO, MENS_TITULO) VALUES(:ID, :MSG, :TITULO)");
				$sql = $pdo->prepare("INSERT INTO TBL_MENSAGEM(ID_PROC_FK, MENS_DESCRICAO, MENS_TITULO, ID_USUARIO_FK, MENS_DATA) VALUES(:ID, :MSG, :TITULO, :ID_USER, NOW())");
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				$sql->bindValue(":ID",$mensagem->getIdProcFk());
				$sql->bindValue(":MSG",$mensagem->getMensagem());
				$sql->bindValue(":TITULO",$mensagem->getTitulo());
				echo $mensagem->getIdUsuarioFK(). "\n"; 
				$sql->bindValue(":ID_USER",$mensagem->getIdUsuarioFK());
				
				$sql->execute();

				echo "Mensagem incluida com sucesso";

			}catch(PDOException $ex){
				echo $ex->getMessage();
			}
		}

		public function deletaMensagemPorId(Mensagem $mensagem){

			try {
				$pdo = $this->getConnection();
				$sql = $pdo->prepare("DELETE FROM TBL_MENSAGEM WHERE ID_MENSAGEM = :id");
				$sql->bindValue(":id", $mensagem->getId());
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->execute();

				return true;

			} catch (PDOException $e) {
				echo($e->getMessage());
				return false;
			}
		}

		public function deletaProcesso(Processo $processo){
			try {
				$pdo = $this->getConnection();
				$sql = $pdo-> prepare("DELETE FROM TBL_PROCESSO WHERE ID_PROC=:id");

				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->bindValue(":id", $processo->getId());

				$sql->execute();
				return true;

			} catch (PDOException $e) {
				echo $e->getMessage();
				return false;
			}
		}

		public function insereProcesso(Processo $processo){
			try{

				$pdo = $this->getConnection();

				$sql = $pdo->prepare("INSERT INTO TBL_PROCESSO (NOME_PROC, TIPO_PROC, NUMERO_PROC, STATUS_PROC, SITUACAO_ATUAL_PROC, RECLAMANTE_PROC, RECLAMADA_PROC, LOCAL_PROC, VALOR_INDENIZACAO_PROC, VALOR_RISCO_PROC, VALOR_PAGO_PROC, DATA_PGTO_PROC, PEDIDOS_PROC) 
    VALUES (:NOME_PROC, :TIPO_PROC, :NUMERO_PROC, :STATUS_PROC, :SITUACAO_ATUAL_PROC, :RECLAMANTE_PROC,:RECLAMADA_PROC,:LOCAL_PROC,:VALOR_INDENIZACAO_PROC,:VALOR_RISCO_PROC,:VALOR_PAGO_PROC,:DATA_PGTO_PROC,:PEDIDOS_PROC)");

				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->bindValue(":NOME_PROC",$processo->getNome());
				$sql->bindValue(":TIPO_PROC",$processo->getTipo());
				$sql->bindValue(":NUMERO_PROC",$processo->getNumero());
				$sql->bindValue(":STATUS_PROC",$processo->getStatus());
				$sql->bindValue(":SITUACAO_ATUAL_PROC",$processo->getSituacaoAtual());
				$sql->bindValue(":RECLAMANTE_PROC",$processo->getReclamante());
				$sql->bindValue(":RECLAMADA_PROC",$processo->getReclamada());
				$sql->bindValue(":LOCAL_PROC",$processo->getLocalFisico());
				$sql->bindValue(":VALOR_INDENIZACAO_PROC",$processo->getValorIndenizacao());
				$sql->bindValue(":VALOR_RISCO_PROC",$processo->getValorEmRisco());
				$sql->bindValue(":VALOR_PAGO_PROC",$processo->getValorPago());
				$sql->bindValue(":DATA_PGTO_PROC",$processo->getDataPagamento());
				$sql->bindValue(":PEDIDOS_PROC",$processo->getPedidos());

				$sql->execute();
				echo "Processo inserido com sucesso";

			}catch(PDOException $ex){
				echo $ex->getMessage();
			}
		}

		public function selecionaProcessosDoAdvogado($id_advogado){
			try{
				$pdo = $this->getConnection();
				//$sql = $pdo->prepare("SELECT * FROM TBL_PROCESSO WHERE ID_PROC IN (SELECT ID_PROC FROM TBL_ADVOGADO_PROCESSO WHERE ID_ADV = :ID_ADV)");
				$sql = $pdo->prepare("SELECT * FROM TBL_PROCESSO WHERE ID_PROC IN (SELECT APRC.ID_PROC FROM TBL_ADVOGADO AS ADV INNER JOIN TBL_USUARIO AS US ON US.ID_USER = ADV.ID_USER_FK INNER JOIN TBL_ADVOGADO_PROCESSO AS APRC ON ADV.ID_ADV = APRC.ID_ADV WHERE US.ID_USER = :ID_ADV)");
				$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql->bindValue(":ID_ADV",$id_advogado[0]);
				$sql->execute();

				return $sql->fetchAll();
			}catch(PDOException $ex){
				echo $ex->getMessage();
				return null;
			}
		}

		private function getConnection(){
			
			try{
				$pdo = new PDO('mysql:host=localhost;dbname=sistema', 'root', '');

				return $pdo;
			}
			catch(PDOException $ex){
				echo $ex->getMessage();
			}

			return null;
		} 


	}


 ?>