<?php
	session_start();
    if((!isset ($_SESSION['login']) == true) and (!isset ($_SESSION['senha']) == true))
    {
      unset($_SESSION['login']);
      unset($_SESSION['senha']);
      header('location:Login.php');
    }

	include_once("ProcessoController.php");
	include_once("Mensagem.php");

	$processoController = new ProcessoController();
	$mensagem = new Mensagem();

	$id = $_GET['idMensagem'];
	$idProcesso = $_GET['idProcesso'];

	$mensagem->setId($id);

	if($processoController->deletaMensagemPorId($mensagem)){
		echo "Mensagem deletado com sucesso";
		header("location: EditarProcesso.php?id=".$idProcesso);
	}
	else{
		echo "Falha ao deletar mensagem";
	}

?>